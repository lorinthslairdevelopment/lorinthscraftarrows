package me.lorinth.craftarrows.Tasks;

import me.lorinth.craftarrows.Arrows.BloodWellArrowVariant;
import me.lorinth.craftarrows.LorinthsCraftArrows;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class BloodWellArrowTask extends BukkitRunnable {

    private BloodWellArrowVariant variant;
    private Location location;
    private Entity arrow;
    private Entity shooter;
    private long currentDuration;

    public BloodWellArrowTask(BloodWellArrowVariant variant, Location location, Entity arrow, Entity shooter){
        this.variant = variant;
        this.location = location;
        this.arrow = arrow;
        this.shooter = shooter;

        this.runTaskTimer(LorinthsCraftArrows.instance, 0, variant.getInterval());
    }

    @Override
    public void run() {
        currentDuration += variant.getInterval();
        if(currentDuration > variant.getDuration())
            cancel();
        doLogic();
    }

    private void doLogic(){
        for(Entity e : location.getWorld().getNearbyEntities(location, variant.getRadius(), variant.getRadius(), variant.getRadius())){
            if(e instanceof LivingEntity && e.isValid() && e.getEntityId() != shooter.getEntityId()){
                drawLine(location, e.getLocation().add(0, 1, 0));
                ((LivingEntity) e).setHealth(Math.max(((LivingEntity) e).getHealth() - variant.getDamage(), 0));
                ((LivingEntity) e).damage(variant.getDamage(), arrow);
            }
        }
    }

    private void drawLine(Location from, Location to){
        Vector direction = to.toVector().subtract(from.toVector()).normalize().multiply(0.4);
        Location particleLocation = from.clone();
        while(particleLocation.distanceSquared(to) > 1){
            particleLocation = particleLocation.add(direction);

            if(variant.getParticle() == Particle.REDSTONE)
                location.getWorld().spawnParticle(variant.getParticle(), particleLocation, 1, new Particle.DustOptions(Color.RED, 1));
            else
                location.getWorld().spawnParticle(variant.getParticle(), particleLocation, 1);
        }
    }
}
