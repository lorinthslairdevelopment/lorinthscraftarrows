package me.lorinth.craftarrows.Arrows;

import me.lorinth.craftarrows.Constants.ArrowNames;
import me.lorinth.craftarrows.Constants.ConfigPaths;
import me.lorinth.craftarrows.LorinthsCraftArrows;
import me.lorinth.craftarrows.Objects.ConfigValue;
import me.lorinth.craftarrows.Util.Convert;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.type.Ladder;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.ArrayList;

public class LadderArrowVariant extends ArrowVariant{

    private int PlacementDelay;
    private Particle ParticleOnPlacement;
    private int MaxLength;
    private int MaxOffset;
    private boolean OnlyForwards;
    private boolean PlaceInAir;

    private ArrayList<Material> replaceableBlocks = new ArrayList<Material>(){{
        add(Material.AIR);
        add(Material.CAVE_AIR);
        add(Material.GRASS);
        add(Material.TALL_GRASS);
    }};

    public LadderArrowVariant(FileConfiguration config) {
        super(config, ConfigPaths.Recipes + ".", ArrowNames.Ladder, new ArrayList<ConfigValue>(){{
            add(new ConfigValue(ConfigPaths.Recipes + "." + ArrowNames.Ladder + ".PlacementDelay", 2));
            add(new ConfigValue(ConfigPaths.Recipes + "." + ArrowNames.Ladder + ".ParticleOnPlacement", "SMOKE_NORMAL"));
            add(new ConfigValue(ConfigPaths.Recipes + "." + ArrowNames.Ladder + ".MaxLength", 8));
            add(new ConfigValue(ConfigPaths.Recipes + "." + ArrowNames.Ladder + ".MaxOffset", 1));
            add(new ConfigValue(ConfigPaths.Recipes + "." + ArrowNames.Ladder + ".OnlyForwards", true));
            add(new ConfigValue(ConfigPaths.Recipes + "." + ArrowNames.Ladder + ".PlaceInAir", true));
        }});
    }

    @Override
    protected void loadDetails(FileConfiguration config) {
        ArrayList<ConfigValue> configValues = getConfigValues();

        PlacementDelay = Convert.Convert(Integer.class, configValues.get(0).getValue(config));
        ParticleOnPlacement = Particle.valueOf((String) configValues.get(1).getValue(config));
        MaxLength = Convert.Convert(Integer.class, configValues.get(2).getValue(config));
        MaxOffset = Convert.Convert(Integer.class, configValues.get(3).getValue(config));
        OnlyForwards = (boolean) configValues.get(4).getValue(config);
        PlaceInAir = (boolean) configValues.get(5).getValue(config);
    }

    @Override
    public void onShoot(EntityShootBowEvent event) {

    }

    @Override
    public void onEntityHit(ProjectileHitEvent event) {

    }

    @Override
    public void onBlockHit(ProjectileHitEvent event) {
        Block b = event.getHitBlock();
        BlockFace hitFace = event.getHitBlockFace();

        if(hitFace == BlockFace.UP || hitFace == BlockFace.DOWN){
            Vector offset = hitFace == BlockFace.UP ? new Vector(0, 1, 0) : new Vector(0, -1, 0);

            ItemStack ladderArrow = LorinthsCraftArrows.getArrowVariantBySimpleName("Ladder").getRecipe().getItem();
            ladderArrow.setAmount(1);
            b.getWorld().dropItemNaturally(b.getLocation().add(offset), ladderArrow);
            return;
        }

        iterate(0, b, hitFace);
    }

    private void iterate(int i, Block b, BlockFace hitFace){
        if(MaxLength < i) {
            return;
        }
        if(i > 0){
            Block thisB = getNextBlock(b, hitFace);
            if(thisB == null)
                return;
            if(!replaceableBlocks.contains(thisB.getType()))
                return;

            thisB.setType(Material.LADDER);
            Ladder ladder = (Ladder) thisB.getBlockData();
            ladder.setFacing(hitFace);
            thisB.setBlockData(ladder);

            if(ParticleOnPlacement != null){
                b.getWorld().spawnParticle(ParticleOnPlacement, thisB.getLocation().add(0.5, 0.5, 0.5), 10, 0.25, 0.25, 0.25, 0);
            }

            b = thisB.getRelative(0, -1, 0);
        }

        final Block block = b;
        if(PlacementDelay > 0){
            Bukkit.getScheduler().runTaskLater(LorinthsCraftArrows.instance, () -> {
                iterate(i+1, block, hitFace);
            }, PlacementDelay);
        }
        else{
            iterate(i+1, b, hitFace);
        }
    }

    private Block getNextBlock(Block start, BlockFace face){
        BlockFace opposite = face.getOppositeFace();
        if(replaceableBlocks.contains(start.getType()) && !replaceableBlocks.contains(start.getRelative(opposite).getType()))
            return start;

        for(int i=1; i<MaxOffset+1; i++){
            Vector positiveDirection = face.getDirection().multiply(i);
            Vector negativeDirection = face.getDirection().multiply(-i);

            if(!OnlyForwards){
                Block neg = start.getRelative(negativeDirection.getBlockX(), negativeDirection.getBlockY(), negativeDirection.getBlockZ());
                if(replaceableBlocks.contains(neg.getType()) && !replaceableBlocks.contains(neg.getRelative(opposite).getType())){
                    return neg;
                }
            }

            Block pos = start.getRelative(positiveDirection.getBlockX(), positiveDirection.getBlockY(), positiveDirection.getBlockZ());
            if(replaceableBlocks.contains(pos.getType()) && !replaceableBlocks.contains(pos.getRelative(opposite).getType())){
                return pos;
            }

        }

        if(PlaceInAir)
            return start;
        else
            return null;
    }

}
