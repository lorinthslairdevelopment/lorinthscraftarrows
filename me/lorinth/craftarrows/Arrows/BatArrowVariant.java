package me.lorinth.craftarrows.Arrows;

import me.lorinth.craftarrows.Constants.ArrowNames;
import me.lorinth.craftarrows.Constants.ConfigPaths;
import me.lorinth.craftarrows.LorinthsCraftArrows;
import me.lorinth.craftarrows.Objects.ConfigValue;
import me.lorinth.craftarrows.Util.Convert;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Bat;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;

public class BatArrowVariant extends ArrowVariant{

    private double Speed;
    private double Damage;

    public BatArrowVariant(FileConfiguration config) {
        super(config, ConfigPaths.Recipes + ".", ArrowNames.Bat, new ArrayList<ConfigValue>(){{
            add(new ConfigValue(ConfigPaths.Recipes + "." + ArrowNames.Bat + ".Speed", 1.0));
            add(new ConfigValue(ConfigPaths.Recipes + "." + ArrowNames.Bat + ".Damage", 6.0));
        }});
    }

    @Override
    protected void loadDetails(FileConfiguration config) {
        ArrayList<ConfigValue> configValues = getConfigValues();

        Speed = Convert.ConvertToDouble(configValues.get(0).getValue(config));
        Damage = Convert.ConvertToDouble(configValues.get(1).getValue(config));
    }

    @Override
    public void onShoot(EntityShootBowEvent event) {
        Bukkit.getScheduler().runTaskLater(LorinthsCraftArrows.instance, () ->
            new BatTask((Arrow) event.getProjectile(), Speed, Damage), 1);
    }

    @Override
    public void onEntityHit(ProjectileHitEvent event) {

    }

    @Override
    public void onBlockHit(ProjectileHitEvent event) {

    }

    private class BatTask extends BukkitRunnable{

        Entity shooter;
        Vector direction;
        Bat bat;
        double speed;
        double damage;

        private BatTask(Arrow arrow, double speed, double damage){
            this.speed = speed;
            this.damage = damage;
            shooter = (Entity) arrow.getShooter();
            direction = shooter.getLocation().getDirection().normalize().multiply(speed);

            Location spawn = arrow.getLocation();
            spawn.setYaw(shooter.getLocation().getYaw());
            spawn.setPitch(shooter.getLocation().getPitch());

            bat = arrow.getWorld().spawn(spawn, Bat.class);
            bat.setAwake(true);
            bat.setAI(false);
            bat.setInvulnerable(true);
            bat.setGravity(false);
            bat.setGliding(true);

            runTaskTimer(LorinthsCraftArrows.instance, 0, 1);
            arrow.remove();
        }

        @Override
        public void run() {
            bat.teleport(bat.getLocation().add(direction));

            LivingEntity closest = null;
            double distance = 1000;
            for(Entity entity : bat.getNearbyEntities(0.5, 0.5, 0.5)){
                if(entity != shooter && entity instanceof LivingEntity){
                    double entityDistance = entity.getLocation().distanceSquared(bat.getLocation());
                    if(entityDistance < distance){
                        closest = (LivingEntity) entity;
                        distance = entityDistance;
                    }
                }
            }

            if(closest != null){
                cancel();
                closest.damage(damage);
            }
            else if((bat.getLocation().getBlock().getType() != Material.AIR &&
                    bat.getLocation().getBlock().getType() != Material.CAVE_AIR)  || !bat.isValid()){
                cancel();
            }
        }

        @Override
        public void cancel(){
            super.cancel();
            bat.remove();
        }
    }
}
