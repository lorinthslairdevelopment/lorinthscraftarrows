package me.lorinth.craftarrows.Arrows;

import me.lorinth.craftarrows.Constants.ArrowNames;
import me.lorinth.craftarrows.Constants.ConfigPaths;
import me.lorinth.craftarrows.Objects.ConfigValue;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;

import java.util.ArrayList;
import java.util.HashMap;

public class TeleportArrowVariant extends ArrowVariant{

    private HashMap<Entity, Entity> shooters = new HashMap<>();
    private Double damageTaken = 0.0;

    public TeleportArrowVariant(FileConfiguration config){
        super(config, ConfigPaths.Recipes + ".", ArrowNames.Teleport, new ArrayList<ConfigValue>(){{
            add(new ConfigValue(ConfigPaths.Recipes + "." + ArrowNames.Teleport + ".DamageTaken", 1.0d));
        }});
    }

    @Override
    protected void loadDetails(FileConfiguration config) {
        ArrayList<ConfigValue> configValues = getConfigValues();
        damageTaken = (double) configValues.get(0).getValue(config);
    }

    @Override
    public void onShoot(EntityShootBowEvent event) {
        shooters.put(event.getProjectile(), event.getEntity());
    }

    @Override
    public void onEntityHit(ProjectileHitEvent event) {
        if(shooters.containsKey(event.getEntity()))
            teleport(event.getEntity(), event.getHitEntity().getLocation());
            shooters.get(event.getEntity()).teleport(event.getHitEntity());
    }

    @Override
    public void onBlockHit(ProjectileHitEvent event) {
        if(shooters.containsKey(event.getEntity())) {
            Entity shooter = shooters.get(event.getEntity());
            teleport(shooter, event.getHitBlock().getLocation().add(0.5, 1, 0.5), shooter.getLocation().getYaw(), shooter.getLocation().getPitch());
        }
    }

    private void teleport(Entity entity, Location location){
        entity.teleport(location);

        if(entity instanceof LivingEntity && damageTaken > 0){
            ((LivingEntity) entity).damage(damageTaken);
        }
    }

    private void teleport(Entity entity, Location location, float yaw, float pitch){
        location.setYaw(yaw);
        location.setPitch(pitch);

        teleport(entity, location);
    }
}