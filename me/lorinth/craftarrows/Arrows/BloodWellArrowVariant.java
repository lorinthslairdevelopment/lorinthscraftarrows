package me.lorinth.craftarrows.Arrows;

import me.lorinth.craftarrows.Constants.ArrowNames;
import me.lorinth.craftarrows.Constants.ConfigPaths;
import me.lorinth.craftarrows.Objects.ConfigValue;
import me.lorinth.craftarrows.Tasks.BloodWellArrowTask;
import me.lorinth.craftarrows.Util.Convert;
import org.bukkit.Particle;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.projectiles.ProjectileSource;

import java.util.ArrayList;

public class BloodWellArrowVariant extends ArrowVariant{

    private Particle particle;
    private double radius;
    private double damage;
    private long interval;
    private long duration;

    public BloodWellArrowVariant(FileConfiguration config){
        super(config, ConfigPaths.Recipes + ".", ArrowNames.BloodWell, new ArrayList<ConfigValue>(){{
            add(new ConfigValue(ConfigPaths.Recipes + "." + ArrowNames.BloodWell + ".Particle", Particle.REDSTONE));
            add(new ConfigValue(ConfigPaths.Recipes + "." + ArrowNames.BloodWell + ".Radius", 5));
            add(new ConfigValue(ConfigPaths.Recipes + "." + ArrowNames.BloodWell + ".Damage", 1));
            add(new ConfigValue(ConfigPaths.Recipes + "." + ArrowNames.BloodWell + ".Interval", 10));
            add(new ConfigValue(ConfigPaths.Recipes + "." + ArrowNames.BloodWell + ".Duration", 50));
        }});
    }

    @Override
    protected void loadDetails(FileConfiguration config) {
        ArrayList<ConfigValue> configValues = getConfigValues();
        particle = Particle.valueOf((String) configValues.get(0).getValue(config));
        radius = Convert.Convert(Double.class, configValues.get(1).getValue(config));
        damage = Convert.Convert(Double.class, configValues.get(2).getValue(config));
        interval = Convert.Convert(Integer.class, configValues.get(3).getValue(config));
        duration = Convert.Convert(Integer.class, configValues.get(4).getValue(config));
    }

    @Override
    public void onShoot(EntityShootBowEvent event) {

    }

    @Override
    public void onEntityHit(ProjectileHitEvent event) {
        Projectile projectile = event.getEntity();
        ProjectileSource source = projectile.getShooter();

        new BloodWellArrowTask(this, projectile.getLocation().add(0, 0.5, 0), projectile, source instanceof Entity ? (Entity) source : null);
    }

    @Override
    public void onBlockHit(ProjectileHitEvent event) {
        Projectile projectile = event.getEntity();
        ProjectileSource source = projectile.getShooter();

        new BloodWellArrowTask(this, projectile.getLocation().add(0, 0.5, 0), projectile, source instanceof Entity ? (Entity) source : null);
    }

    public Particle getParticle(){
        return particle;
    }

    public double getRadius(){
        return radius;
    }

    public double getDamage(){
        return damage;
    }

    public long getInterval(){
        return interval;
    }

    public long getDuration(){
        return duration;
    }
}
