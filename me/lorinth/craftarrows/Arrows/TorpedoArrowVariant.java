package me.lorinth.craftarrows.Arrows;

import me.lorinth.craftarrows.Constants.ArrowNames;
import me.lorinth.craftarrows.Constants.ConfigPaths;
import me.lorinth.craftarrows.LorinthsCraftArrows;
import me.lorinth.craftarrows.Objects.ConfigValue;
import me.lorinth.craftarrows.Util.Convert;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Arrow;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;

public class TorpedoArrowVariant extends ArrowVariant {

    private boolean AirGravity;
    private boolean WaterGravity;
    private double MinSpeed;
    private double MaxSpeed;
    private double AddedAirGravity;

    public TorpedoArrowVariant(FileConfiguration config) {
        super(config, ConfigPaths.Recipes + ".", ArrowNames.Torpedo, new ArrayList<ConfigValue>(){{
            add(new ConfigValue(ConfigPaths.Recipes + "." + ArrowNames.Torpedo + ".AirGravity", true));
            add(new ConfigValue(ConfigPaths.Recipes + "." + ArrowNames.Torpedo + ".WaterGravity", false));
            add(new ConfigValue(ConfigPaths.Recipes + "." + ArrowNames.Torpedo + ".MinSpeed", 0.2));
            add(new ConfigValue(ConfigPaths.Recipes + "." + ArrowNames.Torpedo + ".MaxSpeed", 1.0));
            add(new ConfigValue(ConfigPaths.Recipes + "." + ArrowNames.Torpedo + ".AddedAirGravity", 0.1));
        }});
    }

    @Override
    protected void loadDetails(FileConfiguration config) {
        ArrayList<ConfigValue> configValues = getConfigValues();
        AirGravity = (boolean) configValues.get(0).getValue(config);
        WaterGravity = (boolean) configValues.get(1).getValue(config);
        MinSpeed = Convert.Convert(Double.class, configValues.get(2).getValue(config));
        MaxSpeed = Convert.Convert(Double.class, configValues.get(3).getValue(config));
        AddedAirGravity = Convert.Convert(Double.class, configValues.get(4).getValue(config));
    }

    @Override
    public void onShoot(EntityShootBowEvent event) {
        new TorpedoTask((Arrow) event.getProjectile(), this);
    }

    @Override
    public void onEntityHit(ProjectileHitEvent event) {

    }

    @Override
    public void onBlockHit(ProjectileHitEvent event) {

    }

    private class TorpedoTask extends BukkitRunnable{

        private Arrow arrow;
        private double speed;
        private TorpedoArrowVariant variant;
        private Vector extraGravity;

        private TorpedoTask(Arrow arrow, TorpedoArrowVariant variant){
            this.arrow = arrow;
            this.speed = arrow.getVelocity().length();
            this.variant = variant;
            this.extraGravity = new Vector(0, -variant.AddedAirGravity, 0);

            runTaskTimer(LorinthsCraftArrows.instance, 0, 1);
        }

        @Override
        public void run() {
            Material blockType = arrow.getLocation().getBlock().getType();
            if(!arrow.isValid() || arrow.isDead()){
                cancel();
                return;
            }

            if(blockType == Material.WATER){
                arrow.setGravity(variant.WaterGravity);
            }
            else if(blockType == Material.AIR || blockType == Material.CAVE_AIR || blockType == Material.VOID_AIR){
                arrow.setGravity(variant.AirGravity);
                speed = 0;
            }
            else{
                cancel();
            }

            //Override movement logic if the arrow doesn't have gravity
            if(!arrow.hasGravity()){
                if(speed <= variant.MinSpeed)
                    speed = variant.MinSpeed;
                else if (speed <= variant.MaxSpeed)
                    speed += (variant.MaxSpeed-variant.MinSpeed) / 10.0;
                else
                    speed = variant.MaxSpeed;

                Location newLocation = arrow.getLocation();
                Vector direction = arrow.getLocation().getDirection().normalize().multiply(speed);
                direction.setX(-direction.getX());
                direction.setY(-direction.getY());
                arrow.setVelocity(direction);

                newLocation.getWorld().spawnParticle(Particle.WATER_BUBBLE, newLocation, 5, 0.25, 0.25, 0.25, 0);
            } else {
                arrow.setVelocity(arrow.getVelocity().add(extraGravity));
            }
        }

        @Override
        public void cancel(){
            super.cancel();
            arrow.remove();
        }
    }
}
