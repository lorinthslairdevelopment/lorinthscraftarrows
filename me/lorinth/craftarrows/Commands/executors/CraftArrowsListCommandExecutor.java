package me.lorinth.craftarrows.Commands.executors;

import me.lorinth.craftarrows.Arrows.ArrowVariant;
import me.lorinth.craftarrows.GUI.ArrowListMenu;
import me.lorinth.craftarrows.LorinthsCraftArrows;
import me.lorinth.craftarrows.Util.OutputHandler;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CraftArrowsListCommandExecutor implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if(!sender.hasPermission("lca.list") && !sender.hasPermission("craftarrow.list"))
            OutputHandler.PrintError(sender, "You don't have permission to do that");
        else{
            if(sender instanceof Player && args.length == 1 && args[0].equalsIgnoreCase("gui")){
                if(!sender.hasPermission("lca.menu.view") && !sender.hasPermission("craftarrow.menu.view")){
                    OutputHandler.PrintError(sender, "You don't have permission to do that!");
                    return false;
                }
                ArrowListMenu.open((Player) sender);
            }
            else{
                sender.sendMessage("");
                sender.sendMessage(ChatColor.YELLOW + "Craft Arrows List");
                for(ArrowVariant variant : LorinthsCraftArrows.getAllArrowVariants()){
                    sender.sendMessage("- " + variant.getName());
                }
            }
        }

        return false;
    }
}
